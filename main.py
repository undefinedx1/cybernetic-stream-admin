import json
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("cybernetic-stream-000-firebase-adminsdk-x1c0r-d52d145b26.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

units = json.load(open("info.json"))


def set_payments():
    for elem in units:
        paid = {
            "propertyID": elem,
            "amount": units[elem]["value"],
            "name": "nov22",
            "dateAdded": firestore.SERVER_TIMESTAMP,
            "datePaid": firestore.SERVER_TIMESTAMP,
            "paid": True,
            "status": "paid",
            "datesProcessing": {"1": firestore.SERVER_TIMESTAMP},
        }
        unpaid = {
            "propertyID": elem,
            "amount": units[elem]["value"],
            "name": "dec22",
            "dateAdded": firestore.SERVER_TIMESTAMP,
            "paid": False,
            "status": "unpaid",
        }
        db.collection('payments').add(paid)
        db.collection('payments').add(unpaid)


def main():
    delete_collection(db.collection("units"))

    # set_payments()
    set_docs()
    # docs = db.collection("units").stream()
    # for doc in docs:
    #     print(f'{doc.id} => {doc.to_dict()}')
    # print(3)

    # "24327 Kome Rd": {
    #     "value": 1000,
    #     "url": "24327kome.com",
    #     "autopay": false,
    #     "applicationsOpen": false,
    #     "timing": 0,
    #     "price": 0,
    #     "documents": "",
    #     "name": "800 marina blvd",
    #     "emails": ["x@baronbegier.com", "x@samuelbegier.com", "x@xiaoxiabegier.com"]
    # }
def set_docs():
    for elem in units:
        doc_ref = db.collection("units").document(elem)
        doc_ref.set({
            "value": units[elem]["value"],
            "url": units[elem]["url"],
            "autopay": units[elem]["autopay"],
            "applicationsOpen": units[elem]["applicationsOpen"],
            "timing": units[elem]["timing"],
            "price": units[elem]["price"],
            "name": units[elem]["name"],
            "emails": units[elem]["emails"],
            "underDevelopment": units[elem]["underDevelopment"],
            "fullAddress": units[elem]["fullAddress"]
        })
    print("set docs done")


def delete_collection(coll_ref):
    docs = coll_ref.list_documents(page_size=500)
    deleted = 0

    for doc in docs:
        print(f'Deleting doc {doc.id} => {doc.get().to_dict()}')
        doc.delete()
        deleted = deleted + 1

    if deleted >= 500:
        return delete_collection(coll_ref, 500)


def print_mv():
    running_count = 0
    for elem in units:
        running_count += units[elem]["value"]
    print(running_count)


if __name__ == '__main__':
    main()
